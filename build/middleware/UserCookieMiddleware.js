"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function userCookieMiddleware(request, response, next) {
    var _a;
    if (((_a = request.signedCookies) === null || _a === void 0 ? void 0 : _a.sessionID) && request.signedCookies.sessionID.length === 64 &&
        !request.signedCookies.sessionID.includes(' ') && !request.signedCookies.sessionID.match(/^[A-Za-z]+$/)
        && !/^\d+$/.test(request.signedCookies.sessionID)) {
        var sessionID = request.signedCookies.sessionID;
        response.locals.sessionID = sessionID;
        return next();
    }
    else {
        response.sendStatus(403);
        return next('Cookie was required for request but no cookie was found');
    }
}
exports.userCookieMiddleware = userCookieMiddleware;
