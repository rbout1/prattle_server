"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkIfEmpty = function (request, response, next) {
    var _a = request.body, username = _a.username, email = _a.email, password = _a.password;
    if (username !== '' && email !== '' && password !== '') {
        return next();
    }
    else {
        response.sendStatus(400);
        return next(new Error("Register fields can't be empty"));
    }
};
