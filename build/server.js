"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var mongoose_1 = __importDefault(require("mongoose"));
var cookie_parser_1 = __importDefault(require("cookie-parser"));
var crypto_1 = __importDefault(require("crypto"));
var dotenv_1 = __importDefault(require("dotenv"));
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var user_1 = require("./models/user");
var entry_1 = require("./models/entry");
var reply_1 = require("./models/reply");
var session_1 = require("./models/session");
var UserCookieMiddleware_1 = require("./middleware/UserCookieMiddleware");
var StrongParamsMiddleware_1 = require("./middleware/StrongParamsMiddleware");
var ws_1 = __importDefault(require("ws"));
var cors_1 = __importDefault(require("cors"));
dotenv_1.default.config();
var app = express_1.default();
app.use(express_1.default.json());
app.use(cors_1.default({ origin: 'http://localhost:3000' }));
app.use(cookie_parser_1.default(process.env.COOKIE_SECRET));
mongoose_1.default.connect(process.env.ATLAS_CONNECT, { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose_1.default.connection;
db.once('open', function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        console.log('connected to mongodb');
        return [2 /*return*/];
    });
}); });
var port = 4000;
var webSocketServer = new ws_1.default.Server({ port: 5000 });
webSocketServer.on('connection', function (webSocketClient) {
    webSocketClient.on('message', function (post) { return __awaiter(void 0, void 0, void 0, function () {
        var newPost, username, message, user, entry_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    newPost = JSON.parse(post);
                    username = newPost.username;
                    message = newPost.message;
                    if (!(message !== '' || username !== '')) return [3 /*break*/, 4];
                    return [4 /*yield*/, user_1.User.findOne({ username: username })];
                case 1:
                    user = _a.sent();
                    if (!(user === null)) return [3 /*break*/, 2];
                    console.log('Post could not be made');
                    return [3 /*break*/, 4];
                case 2:
                    entry_2 = new entry_1.Entry({
                        message: message,
                        username: username,
                        likes: 0
                    });
                    return [4 /*yield*/, entry_2.save()];
                case 3:
                    _a.sent();
                    webSocketServer.clients.forEach(function (client) {
                        if (client.readyState === ws_1.default.OPEN) {
                            client.send(JSON.stringify(entry_2));
                        }
                    });
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    }); });
});
app.get('/requiredCookieRoute', [UserCookieMiddleware_1.userCookieMiddleware], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var session, user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, session_1.Session.findOne({ sessionID: response.locals.sessionID })];
            case 1:
                session = _a.sent();
                if (!(session !== undefined)) return [3 /*break*/, 3];
                return [4 /*yield*/, user_1.User.findOne({ _id: session.userID })];
            case 2:
                user = _a.sent();
                return [2 /*return*/, response.status(200).send(user.username)];
            case 3: return [2 /*return*/, response.sendStatus(400)];
        }
    });
}); });
app.post('/user', [StrongParamsMiddleware_1.strongParamsMiddleware({
        username: 'string',
        password: 'string',
        email: 'string',
        firstName: 'string',
        lastName: 'string'
    })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, username, password, email, firstName, lastName, newpassword, user, name_1, newUser;
    var _a, _b, _c, _d, _e, _f;
    return __generator(this, function (_g) {
        switch (_g.label) {
            case 0:
                strongParams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                username = (_b = strongParams) === null || _b === void 0 ? void 0 : _b.username;
                password = (_c = strongParams) === null || _c === void 0 ? void 0 : _c.password;
                email = (_d = strongParams) === null || _d === void 0 ? void 0 : _d.email;
                firstName = (_e = strongParams) === null || _e === void 0 ? void 0 : _e.firstName;
                lastName = (_f = strongParams) === null || _f === void 0 ? void 0 : _f.lastName;
                if (!(username !== '' && email !== '' && password !== '')) return [3 /*break*/, 2];
                newpassword = bcryptjs_1.default.hashSync(password, 10);
                user = new user_1.User({
                    email: email,
                    username: username,
                    password: newpassword,
                    firstName: firstName,
                    lastName: lastName
                });
                name_1 = firstName + ' ' + lastName;
                newUser = { email: email, username: username, name: name_1 };
                return [4 /*yield*/, user.save()];
            case 1:
                _g.sent();
                return [2 /*return*/, response.status(200).send(newUser)];
            case 2: return [2 /*return*/, response.sendStatus(400)];
        }
    });
}); });
// This is where our session should be created
app.post('/user/isValid', [StrongParamsMiddleware_1.strongParamsMiddleware({ email: 'string', password: 'string' })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, email, password, user, current_date, random, hash, session, newUser;
    var _a, _b, _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                strongParams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                email = (_b = strongParams) === null || _b === void 0 ? void 0 : _b.email;
                password = (_c = strongParams) === null || _c === void 0 ? void 0 : _c.password;
                if (!(email !== '' || password !== '')) return [3 /*break*/, 3];
                return [4 /*yield*/, user_1.User.findOne({ email: email })];
            case 1:
                user = _d.sent();
                if (!(user && bcryptjs_1.default.compareSync(password, user.password))) return [3 /*break*/, 3];
                current_date = (new Date()).valueOf().toString();
                random = Math.random().toString();
                hash = crypto_1.default.createHash('sha256').update(current_date + random).digest('hex');
                session = new session_1.Session({
                    sessionID: hash,
                    userID: user._id
                });
                return [4 /*yield*/, session.save()];
            case 2:
                _d.sent();
                newUser = { name: user.fullName, username: user.username };
                response.cookie('sessionID', hash, { signed: true, httpOnly: true });
                return [2 /*return*/, response.status(200).send(newUser)];
            case 3: return [2 /*return*/, response.sendStatus(400)];
        }
    });
}); });
app.post('/user/logout', [UserCookieMiddleware_1.userCookieMiddleware], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, session_1.Session.deleteOne({ sessionID: response.locals.sessionID })];
            case 1:
                _a.sent();
                response.clearCookie('sessionID').sendStatus(200);
                return [2 /*return*/];
        }
    });
}); });
app.post('/entry', [StrongParamsMiddleware_1.strongParamsMiddleware({ message: 'string', username: 'string' })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, username, message, user, entry;
    var _a, _b, _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                strongParams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                username = (_b = strongParams) === null || _b === void 0 ? void 0 : _b.username;
                message = (_c = strongParams) === null || _c === void 0 ? void 0 : _c.message;
                if (!(message !== '' || username !== '')) return [3 /*break*/, 5];
                return [4 /*yield*/, user_1.User.findOne({ username: username })];
            case 1:
                user = _d.sent();
                if (!(user === null)) return [3 /*break*/, 2];
                return [2 /*return*/, response.sendStatus(404)];
            case 2:
                entry = new entry_1.Entry({
                    message: message,
                    username: username,
                    likes: 0
                });
                return [4 /*yield*/, entry.save()];
            case 3:
                _d.sent();
                return [2 /*return*/, response.status(201).send(entry._id)];
            case 4: return [3 /*break*/, 6];
            case 5: return [2 /*return*/, response.sendStatus(400)];
            case 6: return [2 /*return*/];
        }
    });
}); });
app.get('/entry', function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var entries, newEntries;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, entry_1.Entry.find({})];
            case 1:
                entries = _a.sent();
                newEntries = entries.map(function (entry) {
                    return {
                        message: entry.message,
                        username: entry.username
                    };
                });
                return [2 /*return*/, response.status(200).send(newEntries)];
        }
    });
}); });
app.post('/reply', [StrongParamsMiddleware_1.strongParamsMiddleware({ message: 'string', entryID: 'string', username: 'string' })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, message, entryID, username, user, reply;
    var _a, _b, _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                strongParams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                message = (_b = strongParams) === null || _b === void 0 ? void 0 : _b.message;
                entryID = (_c = strongParams) === null || _c === void 0 ? void 0 : _c.entryID;
                username = (_d = strongParams) === null || _d === void 0 ? void 0 : _d.username;
                if (!(message !== '')) return [3 /*break*/, 5];
                return [4 /*yield*/, user_1.User.findOne({ username: username })];
            case 1:
                user = _e.sent();
                if (!(user === null)) return [3 /*break*/, 2];
                return [2 /*return*/, response.sendStatus(404)];
            case 2:
                reply = new reply_1.Reply({
                    message: message,
                    likes: 0,
                    creatorID: user._id,
                    entryID: entryID
                });
                return [4 /*yield*/, reply.save()];
            case 3:
                _e.sent();
                return [2 /*return*/, response.sendStatus(201)];
            case 4: return [3 /*break*/, 6];
            case 5: return [2 /*return*/, response.sendStatus(400)];
            case 6: return [2 /*return*/];
        }
    });
}); });
app.get('/', function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, response.send('this is working')];
    });
}); });
app.listen(port, function () { return console.log('server is up'); });
module.exports = app;
