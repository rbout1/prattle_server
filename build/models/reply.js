"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = __importStar(require("mongoose"));
var mongoose_1 = require("mongoose");
var ReplySchema = new mongoose_1.Schema({
    message: {
        type: String,
        validate: {
            message: 'message is under 500 characters',
            validator: function (value) {
                return value.length < 500;
            }
        }
    },
    creatorID: {
        type: String,
        validate: {
            message: 'creatorID is 24 characters',
            validator: function (value) {
                return value.length === 24;
            }
        }
    },
    entryID: {
        type: String,
        validate: {
            message: 'entryID is 24 characters',
            validator: function (value) {
                return value.length === 24;
            }
        }
    },
    likes: {
        type: Number,
        validate: {
            message: 'likes need be greater than or equal to 0',
            validator: function (value) {
                return value >= 0;
            }
        }
    },
});
ReplySchema.pre('validate', function (next) {
    next();
});
exports.Reply = mongoose.model('Reply', ReplySchema);
