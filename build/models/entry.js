"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = __importStar(require("mongoose"));
var mongoose_1 = require("mongoose");
var EntrySchema = new mongoose_1.Schema({
    message: {
        type: String,
        validate: {
            message: 'message needs to be less than 500 characters',
            validator: function (value) {
                return value.length < 500;
            }
        }
    },
    username: {
        type: String,
        validate: {
            message: 'username is required to not be null or undefined',
            validator: function (value) {
                return value !== null && value !== undefined;
            }
        }
    },
    likes: {
        type: Number,
        validate: {
            message: 'likes need to be greater than or equal to 0',
            validator: function (value) {
                return value >= 0;
            }
        }
    },
});
exports.Entry = mongoose.model('Entry', EntrySchema);
