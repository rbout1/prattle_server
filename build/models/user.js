"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = __importStar(require("mongoose"));
var UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        validate: {
            message: 'no numbers allowed in first name',
            validator: function (value) {
                return !/\d/.test(value);
            }
        }
    },
    lastName: {
        type: String,
        validate: {
            message: 'no numbers allowed in last name',
            validator: function (value) {
                return !/\d/.test(value);
            }
        }
    },
    email: {
        type: String,
        validate: {
            message: 'You need an @ in email',
            validator: function (value) {
                return value.includes('@');
            }
        }
    },
    username: {
        type: String,
        validate: {
            message: 'username needs to be less than 30 characters long',
            validator: function (value) {
                return value.length < 30;
            }
        }
    },
    password: {
        type: String,
        validate: {
            message: 'password hash needs to start with $2',
            validator: function (value) {
                return value.substr(0, 2) === '$2';
            }
        }
    }
});
UserSchema.pre('validate', function (next) {
    if (!this.email.includes('@')) {
        next(new Error('Need a @ in email'));
    }
    next();
});
UserSchema.virtual('fullName')
    .get(function () {
    return this.firstName + ' ' + this.lastName;
}).set(function (fullName) {
    if (!fullName.includes(' ')) {
        throw new Error('Full name must have a space between the first and last name');
    }
    var _a = fullName.split(' '), firstName = _a[0], lastName = _a[1];
    this.firstName = firstName;
    this.lastName = lastName;
});
exports.User = mongoose.model('User', UserSchema);
